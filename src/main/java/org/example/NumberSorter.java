package org.example;

import com.sun.org.apache.xalan.internal.xsltc.util.IntegerArray;

import java.util.*;

public class NumberSorter {
    private ArrayList<Integer> numbers;

    public NumberSorter(String input){
        Scanner scanner;

        if(input == null) {
            scanner = new Scanner(System.in);
        } else {
            scanner = new Scanner(input);
        }

        this.numbers = new ArrayList<Integer>();

        while (this.numbers.size() < 10){
            int number;

            try {
                    number = scanner.nextInt();
            } catch (NoSuchElementException e) {
                    break;
            }
            this.numbers.add(number);
        }
    }

    public void sort(){
        Collections.sort(this.numbers);
    }

    public String getNumbers(){
        String retval = "";
        for (int i = 0; i < this.numbers.size(); i++) {
            retval += String.format("%d ", this.numbers.get(i));
        }
        return retval;
    }


}
