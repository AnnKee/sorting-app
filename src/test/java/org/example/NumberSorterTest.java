package org.example;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class NumberSorterTest {
    private String input;
    private String expected;

    public NumberSorterTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void testSorting() {
        NumberSorter sorter = new NumberSorter(this.input);
        sorter.sort();
        assertEquals(sorter.getNumbers(), this.expected);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {"", ""},
                {"42", "42 "},
                {"3 1 4", "1 3 4 "},
                {"6 2 8 45 10 43 12 43 12 10", "2 6 8 10 10 12 12 43 43 45 "},
                {"11 10 9 8 7 6 5 4 3 2 1", "2 3 4 5 6 7 8 9 10 11 "},
        });

    }
}
